import { IsEnum, IsNotEmpty, IsString } from 'class-validator';

import { Priority } from '../types';

export class CreateTodoDto {
  @IsString()
  @IsNotEmpty()
  task: string;

  @IsEnum(Priority)
  priority: Priority;
}
