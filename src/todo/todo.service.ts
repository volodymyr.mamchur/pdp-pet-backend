import { Injectable, InternalServerErrorException, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import * as _ from 'lodash';

import { CreateTodoDto } from './dto/create-todo.dto';
import { UpdateTodoDto } from './dto/update-todo.dto';
import { Todo } from './entities/todo.entity';

@Injectable()
export class TodoService {
  constructor(
    @InjectRepository(Todo) private todoRepository: Repository<Todo>,
  ) {
  }

  async create(createTodoDto: CreateTodoDto): Promise<void> {
    await this.todoRepository.save(createTodoDto);
  }

  async findAll(): Promise<Todo[]> {
    return await this.todoRepository.find();
  }

  async findOne(id: number): Promise<Todo> {
    const result = await this.todoRepository.findOne(id);

    if (_.isEmpty(result)) {
      throw new NotFoundException();
    }

    return result;
  }

  async update(id: number, updateTodoDto: UpdateTodoDto): Promise<void> {
    const result = await this.todoRepository.update({ id }, updateTodoDto);

    if (result.affected === 0) {
      throw new NotFoundException();
    }
  }

  async remove(id: number): Promise<void> {
    const result = await this.todoRepository.delete({ id });

    if (result.affected === 0) {
      throw new InternalServerErrorException({ message: 'Some error message' });
    }
  }
}
