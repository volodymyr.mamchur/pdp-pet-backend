import { Injectable } from '@nestjs/common';
import * as _ from 'lodash';

@Injectable()
export class AppService {
  protected getRandomInt() {
    return Math.floor(Math.random() * 100);
  }

  getHello(): string {
    return 'Hello World!';
  }

  getRandom(): Array<number> {
    const array = [];
    array.length = 10;

    return _.map(array, () => this.getRandomInt());
  }
}
