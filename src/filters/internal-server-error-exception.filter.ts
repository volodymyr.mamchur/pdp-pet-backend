import { ArgumentsHost, Catch, ExceptionFilter, InternalServerErrorException } from '@nestjs/common';
import { Request, Response } from 'express';

@Catch(InternalServerErrorException)
export class InternalServerErrorExceptionFilter implements ExceptionFilter {
  catch(exception: InternalServerErrorException, host: ArgumentsHost) {
    const exceptionStatus = exception.getStatus();
    const exceptionResponse = exception.getResponse();

    const ctx = host.switchToHttp();
    const response = ctx.getResponse<Response>();
    const request = ctx.getRequest<Request>();

    response
      .status(exceptionStatus)
      .json({
        error: exceptionResponse,
        statusCode: exceptionStatus,
        timestamp: new Date().toISOString(),
        path: request.url,
      });
  }
}
